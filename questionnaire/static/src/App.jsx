import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './components/Main';
import AdminPanel from './components/AdminPanel';
import Login from './components/Login';
import { store } from './redux';

import css from './app.sss';

export default () => (
    <Provider store={store}>
        <div className={css.root}>
            <BrowserRouter>
                <Switch>
                    <Route path='/login' component={Login} />
                    <Route path='/edit-questions' component={AdminPanel} />
                    <Route exact path='/' component={Main} />
                </Switch>
            </BrowserRouter>
        </div>
    </Provider>
);
