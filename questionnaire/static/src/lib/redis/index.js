import { httpPost } from '../transport';

export const writeToRedis = ({ groups, mainDesc }) => {
    const result = {};
    result.mainDesc = mainDesc;
    groups.forEach((questions) => {
        questions.data.forEach((question) => {
            result[question.name] = JSON.stringify(question.descriptions)
        })
    })
    return httpPost('/write-redis', result);
}

export const readFromRedis = async ({ groups, mainDesc }) => {
    const keys = [];
    keys.push('mainDesc');
    groups.forEach((questions) => {
        questions.data.forEach((question) => {
            keys.push(question.name);
        })
    })
    const response = await httpPost('/read-redis', keys);
    const data = await response.json();
    groups.forEach((questions) => {
        questions.data.forEach((question) => {
            if (data[question.name]) {
                question.descriptions = JSON.parse(data[question.name]);
            }
        })
    })
    return { mainDesc: data.mainDesc ? data.mainDesc : mainDesc, groups };
}