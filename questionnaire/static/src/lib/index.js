import groups, { mainDesc } from '../data';
import { readFromRedis } from './redis';

export const getQuestionsData = async () => {
    return await readFromRedis({ groups, mainDesc })
}