export const httpPost = (route, data) => {
    return fetch(`/api${route}`, {
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        method: 'post',
        body: JSON.stringify(data)
    });
}


export const httpDelete = (route, data) => {
    return fetch(`/api${route}`, {
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        method: 'delete',
        body: JSON.stringify(data)
    });
}

export const httpGet = (route) => {
    return fetch(`/api${route}`, {
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        method: 'get'
    });
}
