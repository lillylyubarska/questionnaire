import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import groups from '../../../data';


export default class CustomStepper extends React.Component {
    render() {
        const { activeStep, isAllCompleted, steps } = this.props;
        return (
            <Stepper groups={groups} activeStep={activeStep}>
                {
                    steps.map(step => (
                        <Step completed={isAllCompleted}>
                            <StepLabel>{step}</StepLabel>
                        </Step>
                    ))
                }
            </Stepper>
        );
    }
}
