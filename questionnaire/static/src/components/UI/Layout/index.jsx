import React from 'react';
import Card from '@material-ui/core/Card';

import css from './layout.sss';

function Layout({ children }) {
    return (
        <div className={css.root}>
            <Card>
                { children }
            </Card>
        </div>
    )
}

export default Layout;