import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';

function TextArea(props) {
    return (
        <TextField
            fullWidth
            multiline
            rowsMax='10'
            margin='normal'
            {...props}
        />
    );
}

export default TextArea;