import React from 'react';
import css from './admin.sss';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button';
import Question from './Question.jsx';

class Group extends React.Component {
    state = {
        closed: true,
    }

    onGroupNameChange = (name) => {
        const { group } = this.props;
        group.name = name;
        this.forceUpdate();
    }

    onAddQuestion = () => {
        const { group } = this.props;
        group.questions.push({ scale: [], descriptions: [], name: 'no-name' });
        this.forceUpdate();
    }


    onDeleteQuestion = (index) => {
        const { group } = this.props;
        group.questions.splice(index, 1);
        this.forceUpdate();
    }

    onToggleGroup = () => {
        const { closed } = this.state;
        this.setState({ closed: !closed });
    }

    render() {
        const { closed } = this.state;
        const { index, group } = this.props;
        const { name } = group;
        return (
            <Paper className={css.group}>
                <span role="button" className={css.delete} onClick={() => this.props.onDeleteGroup(index)}>✕</span>
                <span
                    role="button" className={closed ? css.toggleDown : css.toggle}
                    onClick={() => this.onToggleGroup()}
                >{closed ? '﹀' : '︿'}
                </span>
                {
                    closed ? <React.Fragment><h3>{`Группа ${name || ''}`}</h3></React.Fragment>
                        : (
                            <React.Fragment>
                                <h3>Имя группы вопросов</h3>
                                <TextField
                                    margin="normal"
                                    onChange={e => this.onGroupNameChange(e.target.value)}
                                    value={group.name}
                                />
                                <h3>Вопросы:</h3>
                                <div className={css.container}>
                                    {
                                        group.questions.map((question, index) => (
                                            <Question
                                                question={question}
                                                index={index}
                                                onDeleteQuestion={index => this.onDeleteQuestion(index)}
                                            />
                                        ))
                                    }
                                </div>
                                <Button
                                    onClick={() => this.onAddQuestion(group)}
                                    variant="contained"
                                    color="primary"
                                >
                                    + Вопрос
                                </Button>
                            </React.Fragment>
                        )
                }
            </Paper>
        );
    }
}

export default Group;
