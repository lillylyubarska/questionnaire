import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

import css from './admin.sss';

class Question extends React.Component {
    onChangeName = (name) => {
        const { question } = this.props;
        question.name = name;
        this.forceUpdate();
    }

    onAddDescription = () => {
        const { question } = this.props;
        question.descriptions.push('');
        this.forceUpdate()
    }

    onChangeDescription = (index, description) => {
        const { question } = this.props;
        question.descriptions[index] = description;
        this.forceUpdate()
    }


    onDeleteDescription = (index) => {
        const { question } = this.props;
        question.descriptions.splice(index, 1);
        this.forceUpdate()
    }


    onAddMark = () => {
        const { question } = this.props;
        question.scale.push('');
        this.forceUpdate();
    }

    onDeleteMark = (index) => {
        const { question } = this.props;
        question.scale.splice(index, 1);
        this.forceUpdate();
    }

    onChangeMark = (index, mark) => {
        const { question } = this.props;
        question.scale[index] = mark;
        this.forceUpdate();
    }

    render() {
        const { question, index } = this.props;
        return (
            <Paper className={css.question}>
                <span className={css.delete} onClick={() => this.props.onDeleteQuestion(index)}>✕</span>
                <h4>Вопрос</h4>
                <TextField
                    margin='normal'
                    value={question.name}
                    onChange={(e) => this.onChangeName(e.target.value)}
                />
                <h4>Варианиты ответов:</h4>
                {
                    question.scale.length > 0 &&
                    <Paper className={css.scale}>
                        {
                            question.scale.map((mark, index) => (
                                <div className={css.mark}>
                                    <span className={css.delete} onClick={() => this.onDeleteMark(index)}>✕</span>
                                    <TextField
                                        margin='normal'
                                        value={mark}
                                        onChange={(e) => this.onChangeMark(index, e.target.value)}
                                    />
                                </div>
                            ))
                        }
                    </Paper>
                }
                <Button
                    onClick={() => this.onAddMark(question)}
                    variant='contained'
                    color='primary'>
                    + Вариант
                </Button>
                <h4>Описания:</h4>
                {
                    question.descriptions.length > 0 &&
                    <Paper className={css.descriptions}>
                        {
                            question.descriptions.map((description, index) => (
                                <div className={css.description}>
                                    <span className={css.delete} onClick={() => this.onDeleteDescription(index)}>✕</span>
                                    <TextField
                                        fullWidth
                                        margin='normal'
                                        value={description}
                                        onChange={(e) => this.onChangeDescription(index, e.target.value)}
                                    />
                                </div>
                            ))
                        }
                    </Paper>
                }
                <Button
                    onClick={() => this.onAddDescription(question)}
                    variant='contained'
                    color='primary'>
                    + Описание
                </Button>

            </Paper>
        );
    }
}

export default Question;
