import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import Layout from '../UI/Layout/index';
import Group from './Group';
import { httpDelete, httpGet, httpPost } from '../../lib/transport';

import css from './admin.sss';

export default class AdminPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
        };
    }

    async componentDidMount() {
        const resp = await httpGet('/groups', this.state.groups);
        const data = await resp.json();
        this.setState({ groups: data });
    }

    onAddGroup = () => {
        this.state.groups.push({ questions: [] });
        this.forceUpdate();
    };


    onDeleteGroup = (index) => {
        const group = this.state.groups[index];
        console.log(group);
        if (group._id) {
            httpDelete('/group', group);
        }
        this.state.groups.splice(index, 1);
        this.forceUpdate();
    };

    saveData = () => {
        httpPost('/groups', this.state.groups);
        alert('Сохранено');
    };

    render() {
        const { state } = this;
        return (
          <Layout>
                <CardContent>
              <h2>Редактирование вопросов</h2>
              {/* <h2>Описание</h2> */}
                    {/* <TextArea */}
                    {/* value={this.state.mainDesc} */}
                    {/* onChange={(e) => this.handleDescChange(e.target.value)} */}
              {/* /> */}
              <div className={css.container}>
                      {
                            this.state.groups.map((group, index) => (
                              <Group group={group} index={index} onDeleteGroup={this.onDeleteGroup} />
                            ))
                        }
                    </div>
              <Button
                      onClick={this.onAddGroup}
                      variant="contained"
                      color="primary"
                    >
                       + Група вопросов
                    </Button>
            </CardContent>
              <CardActions>
                  <Button
                      onClick={this.saveData}
                      variant="contained"
                      color="primary"
                    >
                       Сохранить
                    </Button>
                </CardActions>
            </Layout>
        );
    }
}
