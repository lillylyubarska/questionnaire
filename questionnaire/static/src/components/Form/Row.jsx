import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';

import css from './form.sss';

export default class Row extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        const { group, name } = this.props;
        this.props.onChange(group, name, value);
    }

    render() {
        const { descriptions, showScale } = this.props;
        return (
            <div className={css.controlContainer}>
                <FormLabel component="legend">
                    <div className={css.legend}>
                        {descriptions[0]}
                    </div>
                </FormLabel>
                <RadioGroup
                    name={this.props.name}
                    group={this.props.group}
                >
                    <div className={css.radioContainer}>
                        {
                            this.props.scale.map(mark => (
                                <FormControlLabel
                                    value={mark}
                                    control={
                                        <Radio
                                            color='green'
                                            checked={this.props.value === mark}
                                            onChange={() => this.handleChange(mark)}
                                        />}
                                    label={showScale ? mark : ''}
                                />
                            ))
                        }
                    </div>
                </RadioGroup>
                <FormLabel component="legend">
                    <div className={css.legend}>
                        {descriptions[1]}
                    </div>
                </FormLabel>
            </div>
        );
    }
}
