import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField/TextField';
import Row from './Row';

import css from './form.sss';

class Form extends React.Component {
    render() {
        const { data, mainDesc, groups } = this.props;
        const { currentGroupName, currentGroupId } = data;
        const currentGroup = groups[currentGroupId];
        return (
            <CardContent>
                <h1 className={css.cardTitle}>{currentGroupName}</h1>
                <p>{mainDesc}</p>
                <h4 className={css.nameTitle}>Заполните ФИО</h4>
                <div className={css.nameField}>
                    <TextField
                        value={data.name}
                        onChange={e => this.props.onNameChange(e.target.value)}
                        margin="normal"
                        fullWidth
                    />
                </div>
                {
                    currentGroup.questions.map(question => (
                        <Row
                            {...question}
                            group={currentGroupName}
                            onChange={this.props.answer}
                            value={data[currentGroupName][question.name]}
                        />
                    ))
                }
            </CardContent>
        );
    }
}

export default Form;
