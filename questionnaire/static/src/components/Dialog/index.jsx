import React from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

class CustomDialog extends React.Component {
    render() {
        return (
            <Dialog open={this.props.open} onClose={this.handleClose} aria-labelledby="simple-dialog-title">
                <DialogTitle id="simple-dialog-title">{this.props.title}</DialogTitle>
                <DialogActions>
                    <Button color="primary" onClick={this.props.onClose}>Отменить</Button>
                    <Button variant="contained" color="primary" onClick={this.props.onClose}>ОК</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

CustomDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func,
    selectedValue: PropTypes.string,
};

export default CustomDialog;
