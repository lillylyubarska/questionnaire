import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField/TextField';
import { Redirect } from 'react-router-dom';
import { httpPost } from '../../lib/transport';
import Dialog from '../Dialog/index';
import Layout from '../UI/Layout/index';

import css from './login.sss';

export default class AdminPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            isLoggedIn: false,
            errorDialog: false,
        };
    }

    login = async () => {
        const resp = await httpPost('/login', this.state);
        if (resp.ok) {
            this.setState({ isLoggedIn: true });
        } else {
            this.setState({ errorDialog: true, login: '', password: '' });
        }
    }

    handleChange = (key, value) => {
        this.setState({ [key]: value });
    }

    render() {
        const { login, password, isLoggedIn } = this.state;
        if (isLoggedIn) return <Redirect to='/edit-questions' />;
        return (
            <Layout>
                <CardContent>
                    <h1 className={css.title}>Вход</h1>
                    <div className={css.centerLayout}>
                        <TextField
                            onChange={e => this.handleChange('login', e.target.value)}
                            value={login}
                            className={css.textField}
                        />
                        <TextField
                            type='password'
                            value={password}
                            onChange={e => this.handleChange('password', e.target.value)}
                            className={css.textField}
                        />
                    </div>
                </CardContent>
                <CardActions>
                    <div className={css.centerLayout}>
                        <Button
                            className={css.button}
                            onClick={this.login}
                            variant='contained'
                            disabled={!login || !password}
                            color='primary'>
                           Войти
                        </Button>
                    </div>
                </CardActions>
                <Dialog
                    title='Неправильній логин или пароль!'
                    open={this.state.errorDialog}
                    onClose={() => this.setState({ errorDialog: false })}
                />
            </Layout>
        );
    }
}
