import React from 'react';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import Dialog from '../Dialog/index';
import Layout from '../UI/Layout/index';
import * as actions from '../../redux/action';
import { checkQuestionsFilling, isFirstGroup, isLastGroup } from './utils';
import { httpGet, httpPost } from '../../lib/transport';
import CustomStepper from '../UI/Stepper';
import groups from "../../data";
import { mainDesc } from '../../data';
import Form from '../Form';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: null,
            errorQuestions: false,
            errorName: false,
            errorServer: false,
            mainDesc: null,
            proceed: false,
            pending: false,
        };
    }

    async componentDidMount() {
        const resp = await httpGet('/groups');
        const groups = await resp.json();
        this.props.init(groups);
        this.setState({ groups });
    }

    goNextGroup = async () => {
        const { data } = this.props;
        const errorQuestions = !checkQuestionsFilling(data[data.currentGroupName], 9);
        const errorName = !data.name;
        this.setState({ errorQuestions, errorName });

        if (!errorQuestions && !errorName) {
            if (isLastGroup(data)) {
                this.setState({ pending: true });
                const resp = await httpPost('/answer', data);
                if (resp.ok) {
                    this.setState({ pending: false, proceed: true });
                } else {
                    this.setState({ pending: false, proceed: true, errorServer: true });
                }
            } else {
                console.log('Go to next group');
                window.scrollTo(0, 0);
                this.props.moveGroup(1);
            }
        }
    }

    goPrevGroup = () => {
        this.props.moveGroup(-1);
    }

    render() {
        if (!this.state.groups) return null;
        const { data } = this.props;
        const {
            errorName, errorQuestions, proceed, pending,
        } = this.state;
        const completed = pending || proceed;
        return (
            <Layout>
                <Form
                    groups={this.state.groups}
                    data={data}
                    mainDesc={mainDesc}
                    onNameChange={this.props.setName}
                    answer={this.props.answer}
                />
                <CardActions>
                    {
                        !isFirstGroup(data)
                        && <Button
                            disabled={proceed}
                            variant='contained'
                            color='primary'
                            onClick={this.goPrevGroup}>Назад</Button>
                    }
                    {
                        !isLastGroup(data)
                        && <Button
                            variant='contained'
                            color='primary'
                            onClick={this.goNextGroup}>{ 'Вперед' }</Button>
                    }
                    {
                        isLastGroup(data)
                        && <Button
                            disabled={proceed}
                            variant='contained'
                            color='primary'
                            onClick={this.goNextGroup}>{ pending ? (proceed ? 'Отправка' : 'Отправлено') : 'Отправить' }</Button>
                    }
                </CardActions>
                <CustomStepper
                    steps={this.state.groups.map(g => g.name)}
                    activeStep={this.props.data.currentGroupId}
                    isAllCompleted={completed}
                />
                <Dialog
                    title={errorQuestions ? 'Выберите вариант ответа в каждой строке' : 'Заполните свое имя'}
                    open={errorName || errorQuestions}
                    onClose={() => this.setState({ errorName: false, errorQuestions: false })}
                />
            </Layout>
        );
    }
}

const mapStateToProps = state => ({
    data: state.data,
});

const mapDispatchToProps = {
    ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
