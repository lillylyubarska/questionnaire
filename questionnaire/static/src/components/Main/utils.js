export const checkQuestionsFilling = (answersMap, answerCount) => {
  return Object.keys(answersMap).length === answerCount;
};


export const isLastGroup = (data) => {
  return data.currentGroupId === data.groups.length - 1;
}


export const isFirstGroup = (data) => {
    return data.currentGroupId === 0;
}
