import { combineReducers, createStore } from 'redux';
import data from './dataReducer';

const rootReducer = combineReducers({ data });

export const store = createStore(rootReducer);
