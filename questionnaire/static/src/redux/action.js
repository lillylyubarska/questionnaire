import constants from './constants';

export const answer = (group, question, value) => ({ type: constants.ANSWER, data: { group, question, value } });

export const moveGroup = offset => ({ type: constants.MOVE_GROUP, data: { offset } });

export const setName = name => ({ type: constants.SET_NAME, data: { name } });

export const init = groups => ({ type: constants.INIT, data: { groups } });
