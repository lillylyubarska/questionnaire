const constants = {
    ANSWER: 'ANSWER',
    MOVE_GROUP: 'MOVE_GROUP',
    SET_NAME: 'SET_NAME',
    INIT: 'INIT',
};

export default constants;
