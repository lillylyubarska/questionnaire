import constants from './constants';

const initialState = {
    currentGroupId: 0,
    name: '',
};

const initGroups = (state, groups) => {
    state.groups = [];
    groups.forEach(({ name }) => {
        state[name] = {};
        state.groups.push(name);
    });
    state.currentGroupName = state.groups[0];
    return state;
};

const data = (state = initialState, action) => {
    console.log(action);
    switch (action.type) {
    case constants.INIT: {
        const { groups } = action.data;
        return {
            ...initGroups({ ...state }, groups),
        };
    }
    case constants.ANSWER: {
        const { group, question, value } = action.data;
        return {
            ...state,
            [group]: { ...state[group], [question]: value },
        };
    }
    case constants.MOVE_GROUP: {
        const { offset } = action.data;
        const currentGroupId = (state.currentGroupId + offset) % state.groups.length;
        return {
            ...state,
            currentGroupId,
            currentGroupName: state.groups[currentGroupId],
        };
    }
    case constants.SET_NAME: {
        const { name } = action.data;
        return {
            ...state,
            name,
        };
    }
    default: {
        return state;
    }
    }
};

export default data;
