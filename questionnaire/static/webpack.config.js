const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './index.jsx',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'index.bundle.js',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                use: 'babel-loader',
            },
            {
                test: /\.sss/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader', options: { modules: true, importLoaders: 1, localIdentName: '[name]__[local]--[hash:base64:5]' } },
                    { loader: 'postcss-loader' },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.jsx'],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
    ],
};
