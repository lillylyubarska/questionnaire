from .handlers.view_handler import handler
from .handlers.api_handler import answer, login
from .handlers.api_handler import get_groups, set_groups, delete_group
from .lib.constants import ROUTES


def setup_routes(app):
    router = app.router
    router.add_get(ROUTES['index'], handler)
    router.add_get(ROUTES['admin'], handler)
    router.add_get(ROUTES['login'], handler)
    api_routes = ROUTES['api']
    router.add_post(api_routes['answer'], answer)
    router.add_post(api_routes['login'], login)
    router.add_get(api_routes['groups'], get_groups)
    router.add_post(api_routes['groups'], set_groups)
    router.add_delete(api_routes['group'], delete_group)
