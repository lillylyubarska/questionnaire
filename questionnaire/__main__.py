import aiohttp

from .app import create_app
from .lib.utils.arguments import create_args

args = create_args()

if args.reload:
    import aioreloader
    aioreloader.start()

if __name__ == '__main__':
    app = create_app()
    aiohttp.web.run_app(app, host=args.host, port=args.port)
