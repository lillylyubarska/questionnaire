from pathlib import Path
import yaml

__all__ = 'load_config'


def load_config():
    config_file = Path.joinpath(Path(__file__).parent, 'config.yaml')
    with open(config_file, 'r') as f:
        config = yaml.safe_load(f)
    return config

