from datetime import datetime, timedelta

from aiohttp import web, ClientTimeout
from aiohttp.web import Response, json_response

from questionnaire.lib.constants import ROUTES
from questionnaire.lib.utils import check_login_credentials
from questionnaire.lib.utils.spreadsheet import Spreadsheet
from questionnaire.model.group import Group
from questionnaire.model.utils import remove_group, update_group


async def answer(request):
    data = await request.json()
    config = request.app.config_dict
    spreadsheet = Spreadsheet(client=config.get('sheet_client'), **config.get('spreadsheet'))
    spreadsheet.append_row_to_sheet(data)
    return Response(body="Ok")


async def set_groups(request):
    groups_data = await request.json()
    for group_data in groups_data:
        await update_group(group_data)
    return Response(body='Ok')


async def delete_group(request):
    group_data = await request.json()
    print(group_data)
    await remove_group(group_data.get('name'))
    return Response(body='Ok')

async def get_groups(request):
    groups = await Group.objects.find_all()
    res = [group.as_dict() for group in groups if group.initial_data]
    return json_response(res)


async def login(request):
    config = request.app.config_dict
    auth_cookie_name = config.get('auth_cookie_name')
    data = await request.json()
    if check_login_credentials(config, data):
        response = web.HTTPFound(ROUTES['admin'])
        expires = datetime.now() + timedelta(days=1)
        response.set_cookie(auth_cookie_name, '1', expires=expires)
        return response
    return Response(status=403)
