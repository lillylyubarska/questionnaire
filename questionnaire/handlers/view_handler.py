from aiohttp.web import Response

from questionnaire.lib.utils import get_template


async def handler(request):
    return Response(
        body=get_template(),
        content_type='text/html')
