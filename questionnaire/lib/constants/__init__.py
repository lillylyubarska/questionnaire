ROUTES = {
    'index': '/',
    'admin': '/edit-questions',
    'login': '/login',
    'api': {
        'answer': '/api/answer',
        'login': '/api/login',
        'groups': '/api/groups',
        'group': '/api/group'
    }
}

ERROR_TEXT = 'ERROR OCCURED'
SPREADSHEET_SCOPE = 'https://spreadsheets.google.com/feeds'
DATETIME_FORMAT = '%m/%d/%y %H:%M:%S'
