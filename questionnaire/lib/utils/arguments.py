import argparse


def create_args():
    parser = argparse.ArgumentParser(description="Questionnaire")
    parser.add_argument('--host', help="Host to listen", default="0.0.0.0")
    parser.add_argument('--port', help="Port to accept connections", default=5000)
    parser.add_argument(
        '--reload',
        action="store_true",
        help="Autoreload code on change")
    parser.add_argument("-c", "--config", type=argparse.FileType('r'),
                        help="Path to configuration file")
    args = parser.parse_args()
    return args
