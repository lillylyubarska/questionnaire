from mako.template import Template


def check_login_credentials(config_dict, login_data):
    user_config = config_dict.get('user')
    login = user_config.get('login')
    password = user_config.get('password')
    if login_data.get('login') == login and login_data.get('password') == password:
        return True
    return False


def get_template(name='index.html'):
    return Template(filename="questionnaire/static/" + name, format_exceptions=True).render()

