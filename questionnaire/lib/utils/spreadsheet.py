from pathlib import Path
import datetime
import gspread
from oauth2client.service_account import ServiceAccountCredentials

from ..constants import SPREADSHEET_SCOPE, DATETIME_FORMAT


class Spreadsheet:

    def __init__(self, url, client, start_sheet_id):
        print(url, start_sheet_id)
        self.sheet = client.open_by_url(url).get_worksheet(start_sheet_id)

    @staticmethod
    def init_gspread():
        scope = [SPREADSHEET_SCOPE]
        json_secret = Path.joinpath(Path(__file__).parent, 'secret.json')
        creds = ServiceAccountCredentials.from_json_keyfile_name(json_secret, scope)
        client = gspread.authorize(creds)
        return client

    def create_cells_list(self, data):
        group_list = data.get('groups', [])
        yield datetime.datetime.utcnow().strftime(DATETIME_FORMAT)
        yield data.get('name')
        for group in group_list:
            group_answers = data[group]
            for answer in group_answers.values():
                yield answer

    def append_row_to_sheet(self, row_data):
        cells = [cell_value for cell_value in self.create_cells_list(row_data)]
        self.sheet.append_row(cells)



