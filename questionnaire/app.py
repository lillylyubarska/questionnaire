import asyncio
import logging
from pathlib import Path

from aiohttp import web
from aiohttp_mako import setup
import motorengine

from .lib.utils.spreadsheet import Spreadsheet
from .middlewares import auth, error_middleware
from .routes import setup_routes
from .settings import load_config

log = logging.getLogger(__name__)


def create_app():
    loop = asyncio.get_event_loop()
    app = web.Application(loop=loop, middlewares=[auth])
    app.config_dict = dict()
    app.config_dict.update(load_config())
    app.on_startup.append(on_start)
    app.on_cleanup.append(on_shutdown)
    setup_routes(app)
    app.router.add_static('/static/', path=Path.joinpath(Path(__file__).parent, 'static'))
    setup(app, input_encoding='utf-8', output_encoding='utf-8', default_filters=['decode.utf8'])
    return app


async def on_start(app):
    config = app.config_dict
    config['sheet_client'] = Spreadsheet.init_gspread()
    db_config = config.get('db')
    motorengine.connect(db_config['name'], **db_config)


async def on_shutdown(app):
    motorengine.disconnect()
