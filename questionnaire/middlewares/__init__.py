from aiohttp import web

from ..lib.constants import ROUTES, ERROR_TEXT


@web.middleware
async def auth(request, handler):
    if request.path == ROUTES['admin']:
        config = request.app.config_dict
        if request.cookies.get(config.get('auth_cookie_name'), None):
            return await handler(request)
        else:
            return web.HTTPFound(ROUTES['login'])
    else:
        return await handler(request)


@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        status = response.status
        if response.status != 404:
            return response
        message = response.message
    except web.HTTPException as ex:
        status = 500
        message = ex.reason
    except:
        status = 500
        message = ERROR_TEXT
    return web.Response(status=status, text=message)
