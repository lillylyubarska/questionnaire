from motorengine import StringField, Document, ListField, BaseField


class Question(Document):
    name = StringField(required=True)
    descriptions = ListField(StringField(), required=True)
    scale = ListField(BaseField(), required=True)

    @staticmethod
    def create_instance(question_data):
        return Question(**question_data)


