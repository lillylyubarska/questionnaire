from bson import ObjectId

from motorengine import StringField, Document, EmbeddedDocumentField, ListField, BaseField

from questionnaire.model.question import Question


class Group(Document):
    name = StringField(required=True)
    questions = ListField(EmbeddedDocumentField(embedded_document_type=Question), required=True)
    initial_data = BaseField()

    @staticmethod
    def get_instance(group_data):
        questions = []
        for item in group_data.get('questions'):
            questions.append(Question.create_instance(item))
        id_str = group_data.get('_id', None)
        _id = ObjectId(id_str) if id_str else None
        return Group(_id=_id, name=group_data.get('name'), questions=questions, initial_data=group_data)

    @staticmethod
    async def delete_instance(name):
        await Group.objects.filter(name=name).delete()

    @staticmethod
    async def update_instance(group_data):
        group = Group.get_instance(group_data)
        upsert = True if group._id else False
        await group.save(upsert=upsert)

    def as_dict(self):
        data = self.initial_data
        if data:
            data.update(_id=str(self._id))
        return data
