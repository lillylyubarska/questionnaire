from bson import ObjectId

from .group import Group
from .question import Question


def get_group(group_data):
    questions = []
    for item in group_data.get('questions'):
        questions.append(Question.create_instance(item))
    id_str = group_data.get('_id', None)
    _id = ObjectId(id_str) if id_str else None
    return Group(_id=_id, name=group_data.get('name'), questions=questions, initial_data=group_data)


async def remove_group(name):
    await Group.objects.filter(name=name).delete()


async def update_group(group_data):
    group = Group.get_instance(group_data)
    upsert = True if group._id else False
    await group.save(upsert=upsert)
