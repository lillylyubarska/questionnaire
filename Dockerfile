FROM busybox:latest
FROM python:3.7
ADD . /opt/questionnaire
RUN pip install -r /opt/questionnaire/requirements.txt

WORKDIR /opt/questionnaire
CMD ["bash", "-c", "python -m questionnaire --reload"]

